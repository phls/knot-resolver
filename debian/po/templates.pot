# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the knot-resolver package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: knot-resolver\n"
"Report-Msgid-Bugs-To: knot-resolver@packages.debian.org\n"
"POT-Creation-Date: 2020-09-14 11:52+0200\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=CHARSET\n"
"Content-Transfer-Encoding: 8bit\n"

#. Type: note
#. Description
#: ../knot-resolver.templates:1001
msgid "Upgrading from Knot Resolver < 5.x"
msgstr ""

#. Type: note
#. Description
#: ../knot-resolver.templates:1001
msgid "Knot Resolver configuration file requires manual upgrade."
msgstr ""

#. Type: note
#. Description
#: ../knot-resolver.templates:1001
msgid ""
"Up to Knot Resolver 4.x, network interface bindings and service start were "
"done by default via systemd sockets. These systemd sockets are no longer "
"supported, and upgrading from a 3.x version (as in Debian Buster) requires "
"manual action to (re)enable the service."
msgstr ""

#. Type: note
#. Description
#: ../knot-resolver.templates:1001
msgid ""
"Please refer to kresd.systemd(7) and to the /usr/share/doc/knot-resolver/"
"upgrading.html file (from knot-resolver-doc). An online version of the "
"latter is available at https://knot-resolver.readthedocs.io/en/stable/"
"upgrading.html#x-to-5-x"
msgstr ""

#. Type: note
#. Description
#: ../knot-resolver.templates:1001
msgid ""
"For convenience, a suggested networking configuration can be found in the "
"file /var/lib/knot-resolver/.upgrade-4-to-5/kresd.conf.net"
msgstr ""
